const Task = require('../models/Task.js')

// Dummy post route
module.exports.createTask = (reqBody) => {
	let newTask = new Task({
		name: reqBody.name
	})
	return newTask.save().then((savedTask, error) => {
		if(error) {
			return error
		}
		return 'Task created successfully!'
	})
}	


module.exports.getTask = (taskId) => {
	return Task.findById(taskId).then((getTask, error) => {
		if(error) {
			return(error)
		}
		return getTask
	})
}


module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result, error) => {
		if(error) {
			return(error)
		}
		result.status = newContent.status
		return result.save().then((updatedTask, error) => {
			if(error) {
				return(error)
			}
			return updatedTask
		})
	})
}