
const express = require('express')
const TaskController = require('../controllers/TaskController.js')
const router = express.Router()


// Dummy post route
router.post('/create', (req, res) =>{
	TaskController.createTask(req.body).then((task) => res.send(task))
})


// Get a task route
router.get('/:id', (req, res) => {
	TaskController.getTask(req.params.id).then((getTask) => res.send(getTask))
})

// Status update route
router.put('/:id/complete', (req, res) => {
TaskController.updateTask(req.params.id, req.body).then((updatedTask) => res.send(updatedTask))
})

module.exports = router