const express = require('express')
const mongoose = require('mongoose')
const dotenv = require('dotenv')
const taskRoutes = require('./routes/taskRoutes.js')

const app = express()
const port = 3001

dotenv.config()

app.use(express.json())
app.use(express.urlencoded({extended: true}))

mongoose.connect(`mongodb+srv://RuRuSensei:${process.env.MONGODB_PW}@zuitt-bootcamp.5jpfbgk.mongodb.net/S36-Activity?retryWrites=true&w=majority`, 
	{
	useNewUrlParser: true,
	useUnifiedTopology: true
	}
)

let db = mongoose.connection
db.on('error', () => console.error('Connection Error.'))
db.on('open', () => console.log('Connected to MongoDB!'))


app.use('/api/tasks', taskRoutes)

app.listen(port, () => console.log(`Server is running at port: ${port}`))
	